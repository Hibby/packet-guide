# Glossary

| Term | Translation |
|------|-------------|
| AX.25 | The underlying packet radio protocol, sometimes used as a generic term |
| BBS | Bulletin Board System - an email and message service hosted on a nearby mailbox |
| NET/ROM | A routing and discovery layer that makes connecting to nearby and distant stations easier |
| Node | A computer in the network. It can be a full featured system with applications hosted at home or at a nearby site, or it can be a simplified system that only passes messages on behalf others |
| PMS | Personal Message System - a reduced functionality BBS hosted on your home node |
